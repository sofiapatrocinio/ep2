# Exercício Programado 2

# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep2/wikis/Descricao)


## Como usar o projeto
* Execute o comando:

```sh
git clone https://gitlab.com/sofiapatrocinio/ep2.git
```
* Abra o projeto no NetBeans IDE 8.2.


* Execute o projeto executando arquivo ProgramStart.


## Funcionalidades do projeto
- Cadastrar-se;
- Entrar com nome de usuário e senha;
- Procurar pokemons;
- Visualizar dados do pokemon.


## Bugs e problemas
- Não é poossível requisitar pokemon.

### Detalhes

Fotos de detalhes do programa numeradas de 1 a 12 na master.

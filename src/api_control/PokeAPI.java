/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api_control;

import api_model.Pokemon;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



/**
 *
 * @author sofiapatrocinio
 */
public class PokeAPI {
    
    List<Pokemon> pokemons = new ArrayList();
    List<Pokemon> types = new ArrayList();
    List<Pokemon> grass = new ArrayList();
    List<Pokemon> fire = new ArrayList();
    List<Pokemon> water = new ArrayList();
    List<Pokemon> bug = new ArrayList();
    List<Pokemon> normal  = new ArrayList();
    List<Pokemon> poison  = new ArrayList();
    List<Pokemon> electric  = new ArrayList();
    List<Pokemon> ground  = new ArrayList();
    List<Pokemon> fighting  = new ArrayList();
    List<Pokemon> psychic  = new ArrayList();
    List<Pokemon> rock  = new ArrayList();
    List<Pokemon> flying  = new ArrayList();
    List<Pokemon> ghost  = new ArrayList();
    List<Pokemon> ice  = new ArrayList();
    List<Pokemon> dragon  = new ArrayList();
    List<Pokemon> steel  = new ArrayList();
    List<Pokemon> dark  = new ArrayList();
    List<Pokemon> fairy  = new ArrayList();

    public List<Pokemon> getTypes() {
        return types;
    }

    public void setTypes(List<Pokemon> types) {
        this.types = types;
    }

    public List<Pokemon> getGrass() {
        return grass;
    }

    public void setGrass(List<Pokemon> grass) {
        this.grass = grass;
    }

    public List<Pokemon> getFire() {
        return fire;
    }

    public void setFire(List<Pokemon> fire) {
        this.fire = fire;
    }

    public List<Pokemon> getWater() {
        return water;
    }

    public void setWater(List<Pokemon> water) {
        this.water = water;
    }

    public List<Pokemon> getBug() {
        return bug;
    }

    public void setBug(List<Pokemon> bug) {
        this.bug = bug;
    }

    public List<Pokemon> getNormal() {
        return normal;
    }

    public void setNormal(List<Pokemon> normal) {
        this.normal = normal;
    }

    public List<Pokemon> getPoison() {
        return poison;
    }

    public void setPoison(List<Pokemon> poison) {
        this.poison = poison;
    }

    public List<Pokemon> getElectric() {
        return electric;
    }

    public void setElectric(List<Pokemon> electric) {
        this.electric = electric;
    }

    public List<Pokemon> getGround() {
        return ground;
    }

    public void setGround(List<Pokemon> ground) {
        this.ground = ground;
    }

    public List<Pokemon> getFighting() {
        return fighting;
    }

    public void setFighting(List<Pokemon> fighting) {
        this.fighting = fighting;
    }

    public List<Pokemon> getPsychic() {
        return psychic;
    }

    public void setPsychic(List<Pokemon> psychic) {
        this.psychic = psychic;
    }

    public List<Pokemon> getRock() {
        return rock;
    }

    public void setRock(List<Pokemon> rock) {
        this.rock = rock;
    }

    public List<Pokemon> getFlying() {
        return flying;
    }

    public void setFlying(List<Pokemon> flying) {
        this.flying = flying;
    }

    public List<Pokemon> getGhost() {
        return ghost;
    }

    public void setGhost(List<Pokemon> ghost) {
        this.ghost = ghost;
    }

    public List<Pokemon> getIce() {
        return ice;
    }

    public void setIce(List<Pokemon> ice) {
        this.ice = ice;
    }

    public List<Pokemon> getDragon() {
        return dragon;
    }

    public void setDragon(List<Pokemon> dragon) {
        this.dragon = dragon;
    }

    public List<Pokemon> getSteel() {
        return steel;
    }

    public void setSteel(List<Pokemon> steel) {
        this.steel = steel;
    }

    public List<Pokemon> getDark() {
        return dark;
    }

    public void setDark(List<Pokemon> dark) {
        this.dark = dark;
    }

    public List<Pokemon> getFairy() {
        return fairy;
    }

    public void setFairy(List<Pokemon> fairy) {
        this.fairy = fairy;
    }
    
    

    public List<Pokemon> getPokemons() {
        return pokemons;
    }

    public void setPokemons(List<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

	private final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {

		PokeAPI http = new PokeAPI();

		http.sendGet();
                
               
            

	}

	public void sendGet() throws Exception {
            
            for(int i=1; i<100; i++)
            {

		String api = "https://pokeapi.co/api/v2/pokemon/";
		Integer number = i;
                String number_string = number.toString();
                String url= api+number_string+"/";
                
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + url);
		//System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer(); 

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
                String x = response.toString();  
               
                JSONParser parser = new JSONParser();
                JSONObject object = (JSONObject) parser.parse(x);
                
                JSONArray types = (JSONArray) object.get("types");
                Integer size_types = types.size();
                
                JSONArray abilities = (JSONArray) object.get("abilities");
                Integer size_abilities = abilities.size();
                
                String name_pokemon = (String) object.get("name");
                System.out.println("Nome: " + name_pokemon);
                
                 ArrayList<String> abilitiesarray = new ArrayList<String>();
                 
                for(Integer cont1=0; cont1 < size_abilities; cont1++ ){
                JSONObject ability = (JSONObject) abilities.get(cont1);
                //JSONObject type = new JSONObject();
                //type.put("types", type);
                ability = (JSONObject) ability.get("ability");
                String ability_pokemon = ability.get("name").toString();
                abilitiesarray.add(ability_pokemon);
                
                //System.out.println("Ability "+ cont1 + ": " + ability.get("name"));
                }
                
                String height_pokemon = object.get("height").toString();
                //System.out.println("Altura: " + height_pokemon);
                
                String weight_pokemon = object.get("weight").toString();
                //System.out.println("Peso: " + weight_pokemon);
                
                ArrayList<String> typessarray = new ArrayList<String>();
                
                for(Integer cont=0; cont < size_types; cont++ ){
                JSONObject type = (JSONObject) types.get(cont);
                //JSONObject type = new JSONObject();
                //type.put("types", type);
                type = (JSONObject) type.get("type");
                String type_pokemon = type.get("name").toString();
                typessarray.add(type_pokemon);
                
                //System.out.println("Ability "+ cont1 + ": " + ability.get("name"));
                }
                
                Pokemon pokemon = new Pokemon(abilitiesarray, name_pokemon, height_pokemon, weight_pokemon, typessarray);
                pokemons.add(i-1,pokemon);
                 
                }
            
            for(int i=1; i<10; i++){
            if(pokemons.get(i).getTypes().contains("grass")){
                grass.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("fire")){
                fire.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("water")){
                water.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("bug")){
                bug.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("normal")){
                normal.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("poison")){
                poison.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("electric")){
                electric.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("ground")){
               ground.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("fighting")){
                fighting.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("ghost")){
                ghost.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("flying")){
                flying.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("psychic")){
                psychic.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("rock")){
                rock.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("ice")){
                ice.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("dragon")){
                dragon.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("steel")){
                steel.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("dark")){
                dark.add(pokemons.get(i));
            }
            if( pokemons.get(i).getTypes().contains("fairy")){
                fairy.add(pokemons.get(i));
            }
            }
            
           
            

        }
}

    


    
        
        




    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    package api_model;

    import java.util.ArrayList;

    /**
     *
     * @author sofiapatrocinio
     */
    public class Pokemon {
        private ArrayList<Abilities> abilities = new ArrayList();
        private String name;
        private String weight;
        private String height;
        private ArrayList<Types> types = new ArrayList();

    public Pokemon(ArrayList abilities, String name, String weight, String height, ArrayList types){
        setAbilities(abilities);
        setName(name);
        setWeight(weight);
        setHeight(height);
        setTypes(types);

    }

        public ArrayList<Abilities> getAbilities() {
            return abilities;
        }

        public void setAbilities(ArrayList<Abilities> abilities) {
            this.abilities = abilities;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public ArrayList<Types> getTypes() {
            return types;
        }

        public void setTypes(ArrayList<Types> types) {
            this.types = types;
        }
    }
    

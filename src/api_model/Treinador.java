/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api_model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author sofiapatrocinio
 */
public class Treinador{
    
    public String nome;
    private String email;
    public String senha;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
            
    public String salvar(){
        
        try {
            FileWriter fw = new FileWriter("treinadores.txt", true);
            PrintWriter pw = new PrintWriter(fw);
            pw.println(this.nome);
            pw.println(this.email);
            pw.println(this.senha);
            pw.flush();
            pw.close();
            fw.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Treinador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "Treinador cadastrado com Sucesso!";
    }

    public void pesquisar() throws IOException {
        Scanner scanner = new Scanner(new FileReader("treinadores.txt"));
        while (scanner.hasNext()) {
        nome = scanner.next();
        email = scanner.next();
        senha = scanner.next();
        //System.out.println(nome);
        //System.out.println(senha);

}
}
    }
    



 

                
                
    

